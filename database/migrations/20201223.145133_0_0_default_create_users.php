<?php

namespace Migration;

use Spiral\Migrations\Migration;

class OrmDefault27cb9b4cf37ad67187a1561b0e41fb2b extends Migration
{
    protected const DATABASE = 'default';

    public function up(): void
    {
        $this->table('users')
            ->addColumn('id', 'primary', [
                'nullable' => false,
                'default'  => null
            ])
            ->addColumn('user_name', 'string', [
                'nullable' => false,
                'default'  => null,
                'size'     => 32
            ])
            ->addColumn('user_surname', 'string', [
                'nullable' => false,
                'default'  => null,
                'size'     => 32
            ])
            ->setPrimaryKeys(["id"])
            ->create();
    }

    public function down(): void
    {
        $this->table('users')->drop();
    }
}
