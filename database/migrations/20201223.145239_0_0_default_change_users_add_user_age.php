<?php

namespace Migration;

use Spiral\Migrations\Migration;

class OrmDefault3692fff705997b0557dbc4a9e974106e extends Migration
{
    protected const DATABASE = 'default';

    public function up(): void
    {
        $this->table('users')
            ->addColumn('user_age', 'string', [
                'nullable' => false,
                'default'  => null,
                'size'     => 32
            ])
            ->update();
    }

    public function down(): void
    {
        $this->table('users')
            ->dropColumn('user_age')
            ->update();
    }
}
