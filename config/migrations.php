<?php

declare(strict_types=1);

return [
    'directory' => __DIR__ . '/../database/migrations/',
    'table' => 'migrations'
];
