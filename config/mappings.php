<?php

declare(strict_types=1);

use App\Models\User;
use Cycle\ORM\Mapper\Mapper;
use Cycle\ORM\Schema;

return [
    'user' => [
        Schema::ENTITY => User::class,
        Schema::MAPPER => Mapper::class,
        Schema::DATABASE => 'default',
        Schema::TABLE => 'user',
        Schema::PRIMARY_KEY => 'id',
        Schema::COLUMNS => [
            'id' => 'id',
            'name' => 'name',
        ],
        Schema::TYPECAST => [
            'id' => 'int',
            'name' => 'string'
        ],
        Schema::RELATIONS => []
    ]
];
