<?php

use Spiral\Database\Driver\MySQL\MySQLDriver;

return [
    'default' => 'default',
    'databases' => [
        'default' => ['connection' => env('DB_CONNECTION')]
    ],
    'connections' => [
        'mysql' => [
            'driver' => MySQLDriver::class,
            'options' => [
                'connection' => 'mysql:host=' . env('DB_HOST') . ';dbname=' . env('DB_DATABASE'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
            ]
        ],
    ]
];
