<?php

use App\Models\User;
use Cycle\ORM\Transaction;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Transaction $transaction) {
    $user = new User(1, 'Jack');
    $transaction->persist($user);
    $transaction->run();
    var_dump($transaction);
});
