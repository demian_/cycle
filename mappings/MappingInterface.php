<?php

declare(strict_types=1);

namespace Mappings;

interface MappingInterface
{
    public function map(): void;
}
