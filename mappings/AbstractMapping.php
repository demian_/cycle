<?php

declare(strict_types=1);

namespace Mappings;

use Cycle\Schema\Registry;

abstract class AbstractMapping implements MappingInterface
{
    protected Registry $registry;

    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }
}
