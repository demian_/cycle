<?php

declare(strict_types=1);

namespace Mappings;

use App\Models\User;
use Cycle\Schema\Definition\Entity;
use Cycle\Schema\Definition\Field;
use Cycle\Schema\Definition\Relation;

final class UserMapping extends AbstractMapping
{
    public function map(): void
    {
        $entity = new Entity();
        $entity->setRole('user');
        $entity->setClass(User::class);

        $entity->getFields()->set(
            'id', (new Field())->setType('primary')->setColumn('id')->setPrimary(true)
        );

        $entity->getFields()->set(
            'name',
            (new Field())->setType('string(32)')->setColumn('user_name')
        );

        $entity->getFields()->set(
            'surname',
            (new Field())->setType('string(32)')->setColumn('user_surname')
        );

        $entity->getFields()->set(
            'age',
            (new Field())->setType('string(32)')->setColumn('user_age')
        );

        $this->registry->register($entity);

        $this->registry->linkTable($entity, 'default', 'users');
    }
}
