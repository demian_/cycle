<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spiral\Migrations\Migrator;

class InitMigrationsCommand extends Command
{
    protected $signature = 'init:migrations';

    protected $description = 'Init migrations';

    private Migrator $migrator;

    public function __construct(Migrator $migrator)
    {
        parent::__construct();
        $this->migrator = $migrator;
    }

    public function handle()
    {
        $this->migrator->configure();
        return 0;
    }
}
