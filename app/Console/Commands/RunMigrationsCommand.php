<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spiral\Migrations\Migrator;

class RunMigrationsCommand extends Command
{
    protected $signature = 'cycle:run';

    protected $description = 'Run cycle migrations';

    private Migrator $migrator;

    public function __construct(Migrator $migrator)
    {
        parent::__construct();
        $this->migrator = $migrator;
    }

    public function handle()
    {
        if (!$this->migrator->isConfigured()) {
            $this->error('Migrator is not configured.');
            return 0;
        }

        while ($this->migrator->run() !== null)
        {
            $this->info('Migrated!');
        }
        return 0;
    }
}
