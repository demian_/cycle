<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spiral\Migrations\Migrator;

class RollbackMigrationCommand extends Command
{
    protected $signature = 'cycle:rollback';

    protected $description = 'Rollback cycle migration';

    private Migrator $migrator;

    public function __construct(Migrator $migrator)
    {
        parent::__construct();
        $this->migrator = $migrator;
    }

    public function handle()
    {
        $this->migrator->rollback();
        $this->info('Rollback performed.');
        return 0;
    }
}
