<?php

namespace App\Console\Commands;

use Cycle\Migrations\GenerateMigrations;
use Cycle\Schema\Compiler;
use Cycle\Schema\Generator\GenerateRelations;
use Cycle\Schema\Generator\GenerateTypecast;
use Cycle\Schema\Generator\RenderRelations;
use Cycle\Schema\Generator\RenderTables;
use Cycle\Schema\Generator\ValidateEntities;
use Cycle\Schema\Registry;
use Illuminate\Console\Command;
use Mappings\AbstractMapping;
use Mappings\MappingInterface;
use Psr\Container\ContainerInterface;
use Spiral\Migrations\Migrator;

class CreateMigrationsCommand extends Command
{
    protected $signature = 'cycle:create';

    protected $description = 'Create migrations based on mappings';

    private const MAPPINGS_NAMESPACE = "Mappings\\";

    private ContainerInterface $container;

    private Migrator $migrator;

    private Registry $registry;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
        $this->migrator = $this->container->get(Migrator::class);
        $this->registry = $this->container->get(Registry::class);
    }

    public function handle()
    {
        $mappingsDir = base_path() . '/mappings/';
        foreach (array_diff(scandir($mappingsDir), ['.' , '..', 'AbstractMapping.php', 'MappingInterface.php']) as $item) {
            $item = substr($item, 0, -4);
            $item = self::MAPPINGS_NAMESPACE .  $item;
            /** @var AbstractMapping $mapping */
            $mapping = $this->container->get($item);
            $mapping->map();
        }

        $cl = new Compiler();
        $schema = $cl->compile($this->registry, [
            new GenerateRelations(), // generate entity relations
            new ValidateEntities(),  // make sure all entity schemas are correct
            new RenderTables(),      // declare table schemas
            new RenderRelations(),   // declare relation keys and indexes
            new GenerateMigrations($this->migrator->getRepository(), $this->migrator->getConfig()),
            new GenerateTypecast(),  // typecast non string columns
        ]);

        return 0;
    }
}
