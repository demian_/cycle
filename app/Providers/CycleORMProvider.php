<?php

namespace App\Providers;

use Cycle\ORM\Factory;
use Cycle\ORM\Mapper\Mapper;
use Cycle\ORM\ORM;
use Cycle\ORM\ORMInterface;
use Cycle\ORM\Schema;
use Cycle\ORM\Transaction;
use Cycle\Schema\Compiler;
use Cycle\Schema\Generator\GenerateRelations;
use Cycle\Schema\Generator\GenerateTypecast;
use Cycle\Schema\Generator\RenderRelations;
use Cycle\Schema\Generator\RenderTables;
use Cycle\Schema\Generator\ResetTables;
use Cycle\Schema\Generator\SyncTables;
use Cycle\Schema\Generator\ValidateEntities;
use Cycle\Schema\Registry;
use Illuminate\Support\ServiceProvider;
use Spiral\Database\Config\DatabaseConfig;
use Spiral\Database\DatabaseManager;
use Spiral\Migrations\Config\MigrationConfig;
use Spiral\Migrations\Migrator;
use Spiral\Migrations\FileRepository;
use Spiral\Tokenizer\ClassLocator;
use Symfony\Component\Finder\Finder;

class CycleORMProvider extends ServiceProvider
{
    public function register()
    {
        $this->bindDatabaseManager();
        $this->bindORMInterface();
        $this->bindTransaction();
        $this->bindClassLocator();
        $this->bindMigrator();
        $this->bindSchemaCompiler();
        $this->bindRegistry();
    }

    private function bindDatabaseManager()
    {
        $this->app->bind(DatabaseManager::class, function () {
            return new DatabaseManager(
                new DatabaseConfig(config('database'))
            );
        });
    }

    private function bindORMInterface()
    {
        $this->app->bind(ORMInterface::class, function ($app) {
            return (new ORM(new Factory($app->get(DatabaseManager::class))))
                ->withSchema(new Schema(
                    (new Compiler())->compile($app->get(Registry::class),
                        [
                            new ResetTables(),       // re-declared table schemas (remove columns)
                            new GenerateRelations(), // generate entity relations
                            new ValidateEntities(),  // make sure all entity schemas are correct
                            new RenderTables(),      // declare table schemas
                            new RenderRelations(),   // declare relation keys and indexes
                            new SyncTables(),        // sync table changes to database
                            new GenerateTypecast(),  // typecast non string columns
                        ])
                ));
        });
    }

    private function bindMigrator()
    {
        $this->app->singleton(MigrationConfig::class, function () {
            return new MigrationConfig(config('migrations'));
        });

        $this->app->bind(Migrator::class, function ($app) {
            return new Migrator($app->get(MigrationConfig::class),
                $app->get(DatabaseManager::class),
                new FileRepository($app->get(MigrationConfig::class))
            );
        });
    }

    private function bindSchemaCompiler()
    {
        $this->app->bind(Compiler::class);
    }

    private function bindClassLocator()
    {
        $this->app->bind(Finder::class, function () {
            return (new Finder())->files()->in([__DIR__ . '/../Models']);
        });

        $this->app->bind(ClassLocator::class, function ($app) {
            return new ClassLocator($app->get(Finder::class));
        });
    }

    private function bindTransaction()
    {
        $this->app->bind(Transaction::class, function ($app) {
            return new Transaction($app->get(ORMInterface::class));
        });
    }

    private function bindRegistry()
    {
        $this->app->singleton(Registry::class, function ($app) {
            return new Registry($app->get(DatabaseManager::class));
        });
    }
}
